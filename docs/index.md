# Home

This website documents the Paroli project: how to set up your own instance, how to update it, how to use it, and how it can be expanded in the future.

## Getting Started

1. Follow the steps on [the prerequisites page](./deployment/1-prerequisites) to ensure that you have the required infrastructure and details needed to deploy.
2. [Deploy the FreeSWITCH system through the Ansible playbook](./deployment/2-deploy-fs).
3. [Deploy the Firebase Functions API](./deployment/3-deploy-functions.md).
4. [Deploy the Paroli website](./deployment/4-deploy-site.md).
5. [Upload required audio files](./deployment/5-add-recordings.md)

## System Guides and Documentation

- [How to get around and interact with FreeSWITCH](./guides/1-understanding-fs). Includes an overview of the key directories and files, and tips on interacting with the system via fs_cli.
