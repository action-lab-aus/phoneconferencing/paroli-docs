# Allowing others to create accounts

If you've just deployed Paroli, you've probably found that there are restrictions on who is allowed to create a new account. As calls can quickly cost significant amounts of money, Paroli limits who can create an account and use the service. Only people whose emails are marked as 'allowed' are able to create accounts and make calls.

_Note: This page assumes you already have access to a created account which has been granted an admin token. For details on creating the first account, or how to grant another account an admin token, see [here](../deployment/4-deploy-site.md)._

User accounts which have been granted admin tokens are able to access hidden pages and features within the Paroli website. One of these pages allows you to modify which email addresses are allowed to create new accounts on your Paroli service. To access it, simply log into the website with an admin account. You can either use the 'Admin Controls' menu at the top of the page and select 'Manage allowed emails', or go to the path `/admin/allowed` (e.g. `www.myparolisite.com/admin/allowed`). If you aren't logged in as an admin the 'Admin Controls' menu will not show, and navigating to that path will be redirect you back to the main Paroli dashboard.

If you have access to the page, you will see a list of allowed email addresses (if it's a new deployment, probably just yours) and these two fields:

![Allowing new email addresses fields](../img/allowed.png)

These fields will allow you to grant particular email addresses the ability to create an account. Use them like so:

## Adding Domains

The 'Add Domain' field allows anyone with an email address that matches the entered domain to create an account. For example, entering `monash.edu` will allow addresses such as `dan.richardson@monash.edu` and `adnan.islam@monash.edu` to use the system. **Be sure that any domains you enter are not publicly available for registration.** For instance, be sure not to enter domains such as `gmail.com` or `outlook.com`, as anyone would be able to create one of these addresses and easily gain access to your system within minutes.

## Adding Individual Email Addresses

To support people who use these public domains being able to use the service, you can also explicitly enter individual email addresses. Simply enter an address into the field and press `Add Email Address`.

_Note: unlike when you entered the email address for the first account directly into the database, you can enter addresses into this field normally---the system will convert any periods into commas for you._
