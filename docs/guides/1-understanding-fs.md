# Getting to know Paroli's FreeSWITCH server

FreeSWITCH is a complicated beast, and if you're learning both how it and the Paroli codebase work at once you'll have a lot on your plate. The [FreeSWITCH documentation](https://developer.signalwire.com/freeswitch/FreeSWITCH-Explained/) is also a mixed bag. This page will give an overview of how things work, so that hopefully you're not too lost.

## Key FreeSWITCH folders & files

The FreeSWITCH installation can be found at the following path:

```path
/usr/local/freeswitch
```

The most important subdirectories are:

- `bin`: Contains a number of essential programs, including fs_cli (discussed [below](#using-fs_cli)) and FreeSWITCH itself  
- `conf`: Contains all of the configuration files that affect FreeSWITCH's behaviour.
- `log`: Contains the logs of all FreeSWITCH activities, such as calls and errors.
- `recordings`: The default storage location for call recordings.
- `sounds`: Has language-specific subdirectories containing sounds that are played back to users during calls.

Of these, you will probably spend the most time in `conf`. Of its contents, these are the most important subdirectories and files:

### autoload_configs/

Contains configuration files for FreeSWITCH's many features and modules. The only one you're likely to engage with is `conference.conf.xml`, which defines the profiles that can be assigned to users during conference calls (setting things like what button controls are available, and what sounds should play for aprticular events).

### dialplan/

FreeSWITCH's logic is traditionally defined in 'dialplans': programmatic instructions written in XML which can assess conditions, access and set variables, and perform actions during a call. Particular parts of a dialplan are accessed by dialing their defined extension. The call's current destination is assessed against each endpoint in the dialplan linearly through regex.

For example, the following dialplan endpoint will be entered when the user calls/is forwarded to the 9198 extension, and will play the Tetris theme 10 times:

```xml
<extension name="tone_stream">
    <condition field="destination_number" expression="^9198$">
        <action application="answer"/>
        <action application="playback" data="{loops=10}tone_stream://path=${conf_dir}/tetris.ttml"/>
    </condition>
</extension>
```

Different dialplan files are loaded based on the caller's context. Paroli uses the default context, and its dialplan logic overwrites the majority of the unused boilerplate content in `default.xml`.

### directory/

Directory houses the user accounts that can log into the FreeSWITCH system. Paroli removes all of the default user accounts bar one, which is used for WebRTC. If you add any other users, make sure to secure them with a password.

### freeswitch.xml

Core config file for FreeSWITCH, telling the program where to find the other config files. The only thing that gets edited in here is the supported languages.

### ivr_menus/

Contains the XML logic for any IVR menus you want to include, similar to the dialplans. Paroli's are stored in `paroli_ivr.xml`.

### lang/

Contains the details for each language noted as being supported in `freeswitch.xml`. Each language has its own subfolder, containing an XML file defining the location of the language's sound files.

### sip_profiles/

Contains the details for connecting to the FreeSWITCH system over SIP. The internal profile handles logging into the system through accounts (i.e. WebRTC for Paroli), and the external profile is used for connecting with SIP trunks to access the PSTN. Paroli defines its trunk connections in `sip_profiles/external/pstn_gateway.xml`.

### vars.xml

Defines the main variables utilised by FreeSWITCH at runtime. One of the most important is the default password, which should have already been changed during deployment. The other settings touched in here relate to SSL, to enable WebRTC.

## Using fs_cli

FreeSWITCH runs as a daemon process on the server. To access it, we use the FreeSWITCH command line interface, `fs_cli`. This is a program that is located at `/usr/local/freeswitch/bin/fs_cli`. You're likely to be using it a lot, so I recommend adding it as an alias to avoid typing that out every time:

Add the following line to `~/.bashrc`:

```bash
alias fs_cli="/usr/local/freeswitch/bin/fs_cli"
```

and then run:

```bash
source ~/.bashrc
```

Now you can type `fs_cli` from any location to access the FreeSWITCH command line interface.

Once in the cli, you can view the system logs in real time and directly execute a wide number of commands. Here are some examples:

- `reloadxml`: If you have edited any XML (e.g. a dialplan), you'll have to either restart FreeSWITCH or run this for it to take effect.
- `sofia profile external rescan`: Run this after running `reloadxml` to make FreeSWITCH try to reload any changes you've made to the SIP trunk config.
- `console loglevel 7`: Sets what logs should be printed to the console. Values are: 0 "CONSOLE", 1 "ALERT", 2 "CRIT", 3 "ERR", 4 "WARNING", 5 "NOTICE", 6 "INFO", 7 "DEBUG"
- `eval ${sound_prefix}`: Use the eval command to see the current value of a variable.
- `originate {origination_caller_id_number=TRUNK_NUM}sofia/gateway/TRUNK_NAME/CLIENT_NUM 9198`: Uses the given trunk details to call a client, and routes them to the Tetris endpoint in the dialplan.

Press CTRL + D to exit `fs_cli`
