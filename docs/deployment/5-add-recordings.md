# Adding Audio Recordings

As it is an IVR system, Paroli users will primarily engage with it over the phone: listening to audio recordings and pressing buttons. As such, a number of audio files are required to play to the user to describe menus, interactions, events, and to provide feedback. These files are stored in Firebase. A list containing the filenames and contents of each recording is in the FreeSWITCH project, in the file [audioFilesList.json](https://gitlab.com/action-lab-aus/phoneconferencing/freeswitch-node/-/blob/master/audioFilesList.json). Each time the FreeSWITCH server starts, it will use this list to check if any audio files are missing, and try to download them. Multiple versions of each file are stored (8000Hz, 16000Hz, and 32000Hz), to support the best possible playback for the current call quality without needing transcoding. The files are stored in WAV format, further reducing processing load on the server during playback.

In the future, a page will be added to the admin menu on the website to support direct uploading and modification of these files through the GUI. Currently, however, they need to be added to Firebase Storage manually.

If you don't want to make your own recordings, you can download our files here (Make sure to unzip these before uploading them):

- [English (en)](https://mega.nz/file/6QIHzTSL#iFwbq92LGn-m7Gcwj8gZn96TNLdj2cApa_inW7TWmdk) (up to date as of 13/04/2023)
- [Bangla (bn)](https://mega.nz/file/6FpBnDbJ#0VS63-e16VS1swMW8HAOloMim6Ff3BkQUHSpoOZHtig) (up to date as of 13/04/2023)
- [Nepali (ne)](https://mega.nz/file/qEYxHJ7I#u3iCseowspRBS41je65SSBj3teirYZY2nbopYMo7-ww) (up to date as of 17/04/2023)

If there have been updates to audioFilesList.json since the above dates, you may need to create additional recordings. Obviously, if you want to support other languages you will also need to include them.

Once you have all of the required audio files in 8000Hz, 16000Hz, and 32000Hz WAV format, you need to upload them to Google's servers. Files are stored within buckets using the following path format:

`/audio/[LANG]/[QUALITY]/[FILENAME].wav`

For example:

`/audio/en/16000/raised_hand.wav`

If you're only updating one or two files, doing that through the Storage section of the Firebase Console is very easy. However, if you're uploading whole folders (e.g. using the links above), doing it through the Google Cloud console is much quicker:

1. Go to [https://console.cloud.google.com/storage](https://console.cloud.google.com/storage)
2. Select your project from the dropdown at the top of the page.
3. Choose the storage bucket ending in appspot.com which _doesn't_ start with 'staging'.
4. In that bucket, create a folder called 'audio'. Enter that new folder.
5. Click 'Upload folder' to add each required language folder. Subfolders and files will be included in the upload. Make sure that each top folder's name uses the same language code (e.g. English = 'en') as the one you used in your Ansible inventory.
6. Restart the Paroli node app on your FreeSWITCH server to download the new files:

`sudo pm2 restart paroli`
