# Deploying Firebase Functions

Paroli uses Firebase to host a lightweight, serverless API for use by client-facing interfaces (such as the Paroli website).

1. Set up the latest version of the Firebase CLI by following [these instructions](https://firebase.google.com/docs/functions/get-started#set-up-node.js-and-the-firebase-cli).
2. Clone the Paroli Firebase Functions [repository](https://gitlab.com/action-lab-aus/phoneconferencing/BD_Conference), like so:

        git clone https://gitlab.com/action-lab-aus/phoneconferencing/BD_Conference

3. Log into the Firebase CLI. Start the process with `firebase login`, and follow the instructions.
4. Make sure you're using the correct Firebase project. Run `firebase projects:list` to see a list of all available projects. If your Paroli project is not marked as current, run:

        firebase use PROJECT_ID

5. The API needs to know a few details about your Paroli deployment. This is done by adding runtime config through the Firebase CLI. You can set a variable like so:

        firebase functions:config:set deployment.sendgridapi="THE API KEY"
    These are the values you need to enter (can also be found in `/functions/.runtimeconfig.json`). Each is preceded by 'deployment'.

    - `number`: The phone number of one of your trunks, to use as a default backup.
    - `sendgridapi`: Your Sendgrid API key, [set up earlier](./1-prerequisites.md#sendgrid-api-key).
    - `adminemail`: The email address that Sendgrid will use as the 'sender'.
    - `adminpassword`: A password that can be used to elevate users to admins.
    - `website`: The web address that users will be linked to during email verification.
    - `freeswitchuseraddress`: The login address for a WebRTC user (i.e. "1000@YOUR_FREESWITCH_DOMAIN.com")
    - `freeswitchuserpw`: The login password for WebRTC (the 'fs_user_password' variable in your FreeSWITCH deployment YAML)
    - `freeswitchws`: The websocket address for your FreeSWITCH deployment (i.e. "wss://YOUR_FREESWITCH_DOMAIN.com:7443")

6. Once all variables have been set, deploy the API:

        firebase deploy --only functions

7. Conditions which determine who can access particular data and files are controlled by Firebase Rules. There is one set of rules for the Realtime Database, and one for the Cloud Storage. Both sets of rules are contained within this repository. **Not doing this may leave your recordings and data unsecured.** You should deploy the rules like so:

        firebase deploy --only storage
        firebase deploy --only database

If during development you would like to push an update to a single function, you can do it like so:

    firebase deploy --only functions:scheduleConference

_[Next: Deploy the Paroli website](./4-deploy-site.md)_
