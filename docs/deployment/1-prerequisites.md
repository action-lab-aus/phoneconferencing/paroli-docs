# Prerequisites

Before starting installation, the following steps need to be followed.

## Sendgrid API Key

Paroli uses the SendGrid API as an easy way of confirming users' email addresses, and so to run Paroli unmodified you'll need a free Sendgrid API key. To get one, create an account on [sendgrid.com](https://sendgrid.com). Once logged in, go to Settings -> API Keys. Create an API Key that can at least send emails and make note of it.

## SignalWire token

SignalWire, the owners of FreeSWITCH, recently made it so that you need a token in order to download from the FreeSWITCH repositories. Getting one is free and easy. Follow the steps [on this page](https://developer.signalwire.com/freeswitch/FreeSWITCH-Explained/Installation/HOWTO-Create-a-SignalWire-Personal-Access-Token_67240087/), and make a note of your token.

## Debian server, domain

Paroli uses an open source piece of software called [FreeSWITCH](https://developer.signalwire.com/freeswitch/FreeSWITCH-Explained/Introduction/) as a call management system. The creators of FreeSWITCH recommend that it should be run on Debian, so that's what Paroli is built for. This server needs to have a static IP address, SSH access, a user called 'admin', and ideally a domain name and >= 30GB of storage (each Paroli caller is recorded in WAV format---while recordings get uploaded and cleaned up after calls, these WAVs can still get pretty big, especially with 20+ being recorded at once). The server will be fully configured through an automated deployment script.

An easy way to get set up is with Amazon's Elastic Cloud:

1. Go to Amazon AWS EC2 and select 'Launch Instances'
2. Give the server a name
3. In 'Application and OS Images (Amazon Machine Image)', select 'Debian' under 'Quick Start'
4. In 'Instance Type', select the free t2.micro tier if you're just testing the system out. For production environments, small or medium are more suitable.
5. In the 'Key pair (login)' section, create or select an SSH key pair to access the server.
6. In the 'Network settings' section, create a security group which allows SSH access. You'll need to come back after configuring a SIP trunk to allow communicaiton between it and FreeSWITCH. An example is available [later in this page](#twilio-ec2-security-group).
7. Under 'Configure Storage', give the server at least 30GB of storage (the limit of the free tier).
8. Launch the instance.
9. By default, your instance's IP will change after each reboot. To give it a static IP address, select 'Network & Security -> Elastic IPs'. Click 'Allocate Elastic IP address', then 'Allocate'. Then select the new IP address, and click 'Associate Elastic IP address'. Choose your instance and select 'Associate'.
10. With a static IP, it's also easier to associate your server with a domain name. You will need a domain for your Paroli website, and assigning a subdomain to your FreeSWITCH server (e.g. fs.paroli.live) can be very handy. This can be done with AWS Route 53, by [registering a domain](https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/domain-register.html) and [following these instructions](https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/routing-to-ec2-instance.html).

## Firebase

Paroli heavily relies on Google Firebase for its back-end infrastructure. Firebase's Realtime Database (RTDB) is used to store usage data, as well as manage commands for the FreeSWITCH server; Firebase Authentication is used to manage Paroli user accounts; Firebase Functions is used to host a lightweight API; Firebase Storage is used to host recordings and system audio files; and it's recommended to host the static Paroli website through Firebase Hosting. As the deployment script requires Firebase keys and tokens to set up the Paroli FreeSWITCH installation, setting up a Firebase project is one of the first things you need to do.

1. Navigate to [the Firebase console](https://console.firebase.google.com/).
2. Select 'Add Project', and follow the instructions.
3. Once the project is created, expand the sidebar on the left and select 'All Products'.
4. Select 'Authentication', then 'Get Started'.
5. Select 'Email/Password', enable, then save.
6. Still in 'Authentication', go to the 'Templates' tab, 'Email address verification', and click the edit button. Click 'Customise action URL' and change the address to use your domain, but keep the /\__/auth/action endpoint (i.e. 'https://YOUR_ADDRESS.COM/__/auth/action'). Save.
6. Select 'All Products' again, then 'Realtime Database'.
7. Select 'Create Database'. Select your region, 'Start in locked mode', then 'Enable'.
8. Select 'All Products', then 'Storage'. 'Start in production mode', choose a region, then 'Done'.
9. Select 'All Products', then 'Functions'. To enable Firebase Functions, you'll probably be prompted to upgrade to a different payment plan. The default 'Spark' plan is free, whereas Functions has paid usage so you need a 'Blaze' plan. It will also increase your allowance for Storage and Hosting. The costs are extremely low - cents per month, unless you have very high usage.

We'll set up Firebase Hosting later on.

## SIP Trunk(s)

In order to work, Paroli needs to be able to make and receive phone calls through the standard public switched telephone network (PSTN). It does this through SIP trunks, which act as a bridge between the internet and the PSTN.

Unfortunately, the laws and regulations surrounding access and use of SIP trunks vary wildly between countries. In some countries it is very simple, whereas in others you may need to have a license, utilise particular phone providers, and/or have your server hardwired to the trunk through a dedicated fiber connection. You should investigate these matters in your deployment context before continuing.

As trunks can have a limit of how many simultaneous callers they can handle, or which countries can be dialled, Paroli allows for a single server deployment to be able to connect to multiple trunks. For example, you may want a local trunk for cheaper in-country calls, and another trunk to enable more expensive international ones.

All trunk providers will have their own processes for getting started. However, to get up and running quickly, we recommend creating a SIP trunk through [Twilio](https://www.twilio.com/docs/sip-trunking). Depending on the country you are dialing, Twilio can either be [reasonable](https://www.twilio.com/sip-trunking/pricing/us) or very [expensive](https://www.twilio.com/sip-trunking/pricing/np). It is, however, quick to set up, and can be cancelled without any long-term commitments.

1. Go to [the Twilio console](https://console.twilio.com/) and create a user account and project.
2. Once in your project, click 'Explore Products' and then 'Elastic SIP Trunking'.
3. Select 'Get Started' then 'Create a SIP Trunk'. Call it something like 'paroli-twilio'.
4. Having made your trunk, select 'Numbers' on the left. Select 'Add a number', 'Buy a Number'. It only needs to support voice. Buy a number and assign it to your trunk.
5. Select 'Termination' on the left. Type something unique into the 'Termination SIP URI' field. This URI is what your server will talk to.
6. Still on the 'Termination' page, scroll down to 'Authentication'. The options here help prevent third parties from using the system and driving up your call costs.
7. Click the plus next to 'IP Access Control Lists', and enter your debian server's IP, followed by 32 on the dropdown. This will make it so that only your server can use the trunk.
8. Click the plus next to 'Credential Lists'. Create a new username and password (make sure to remember it, it can't be viewed again). Your server will use these details to authenticate its communications with the trunk.
9. Click 'Origination' on the left. Click the plus next to 'Origination URI', and enter the (sub)domain name of your debian server, with the sip prefix and port 5080 suffix, as so: `sip:subdomain.yourdomain.com:5080`. This will send inbound calls to your server.
10. Remember to update your server's security groups to allow communication between your trunk and your server. If you don't, your FreeSWITCH server will be under constant bombardment by attempted scammers. If you used Twilio and Amazon EC2, [these security group settings worked fine for me](#twilio-ec2-security-group).
11. Done! You'll need the trunk's details during deployment, so make sure to have the phone number, termination SIP uri, and credentials to hand.

_Note: Twilio regards a number of countries and dial codes as being 'risky' to call. Make sure the locations you wish to call [are ticked on this page](https://www.twilio.com/console/voice/calls/geo-permissions), otherwise your calls may be automatically rejected._

### Twilio EC2 Security Group

|IpProtocol | FromPort | ToPort | IpRanges |
| ---- | ---- | ---- | ---- |
|tcp| 80| 80| 0.0.0.0/0|
|tcp| 80| 80| ::/0|
|tcp| 22| 22| 0.0.0.0/0 (SSH)|
|udp| 10000| 20000| 34.203.250.0/23 (Twilio Media North America Virginia)|
|udp| 10000| 20000| 3.104.90.0/24 (Twilio Media Pacific Sydney)|
|udp| 10000| 20000| 54.169.127.128/26 (Twilio Media Pacific Singapore)|
|udp| 10000| 20000| 52.215.127.0/24 (Twilio Media Europe Ireland)|
|udp| 10000| 20000| 54.171.127.192/26 (Twilio Media Europe Ireland)|
|udp| 10000| 20000| 3.112.80.0/24 (Twilio Media Pacific Tokyo)|
|udp| 10000| 20000| 3.122.181.0/24 (Twilio Media Europe Frankfurt)|
|udp| 10000| 20000| 54.252.254.64/26 (Twilio Media Pacific Sydney)|
|udp| 10000| 20000| 54.172.60.0/23 (Twilio Media North America Virginia)|
|udp| 10000| 20000| 35.156.191.128/25 (Twilio Media Europe Frankfurt)|
|udp| 10000| 20000| 54.65.63.192/26 (Twilio Media Pacific Tokyo)|
|udp| 10000| 20000| 18.228.249.0/24 (Twilio Media South America Sao Paulo)|
|udp| 10000| 20000| 54.244.51.0/24 (Twilio Media North America Oregon)|
|udp| 10000| 20000| 177.71.206.192/26 (Twilio Media South America Sao Paulo)|
|udp| 10000| 20000| 3.1.77.0/24 (Twilio Media Pacific Singapore)|
|udp| 5060| 5080| 54.172.60.0/30 (Twilio North America Virginia UDP)|
|udp| 5060| 5080| 54.171.127.192/30 (Twilio Europe Ireland UDP)|
|udp| 5060| 5080| 54.252.254.64/30 (Twilio Asia Pacific Sydney UDP)|
|udp| 5060| 5080| 35.156.191.128/30 (Twilio Europe Frankfurt UDP)|
|udp| 5060| 5080| 54.169.127.128/30 (Twilio Asia Pacific Singapore UDP)|
|udp| 5060| 5080| 54.244.51.0/30 (Twilio North America Oregon UDP)|
|udp| 5060| 5080| 177.71.206.192/30 (Twilio South America Sao Paulo UDP)|
|udp| 5060| 5080| 54.65.63.192/30 (Twilio Asia Pacific Tokyo UDP)|
|tcp| 8675| 8675| 0.0.0.0/0 (Node API reqs)|
|tcp| 8675| 8675| ::/0 (Node API reqs)|
|tcp| 443| 443| 0.0.0.0/0|
|tcp| 443| 443| ::/0|
|tcp| 5060| 5080| 54.244.51.0/30 (Twilio North America Oregon)|
|tcp| 5060| 5080| 54.171.127.192/30 (Twilio Europe Ireland)|
|tcp| 5060| 5080| 35.156.191.128/30 (Twilio Europe Frankfurt)|
|tcp| 5060| 5080| 54.172.60.0/30 (Twilio North America Virginia)|
|tcp| 5060| 5080| 54.252.254.64/30 (Twilio Asia Pacific Sydney)|
|tcp| 5060| 5080| 177.71.206.192/30 (Twilio South America Sao Paulo)|
|tcp| 5060| 5080| 54.65.63.192/30 (Twilio Asia Pacific Tokyo)|
|tcp| 5060| 5080| 54.169.127.128/30 (Twilio Asia Pacific Singapore)|
|tcp| 7443| 7443| 0.0.0.0/0 (SIP over WSS)|

_[Next: Deploying the FreeSWITCH server with Ansible](../2-deploy-fs)_
